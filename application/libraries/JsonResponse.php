<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JsonResponse {

	var $success = false;
	var $data = array();
	var $response;


	public function setValue($success,$data){
		$this->success = $success;
		$this->data=$data;
		$this->response = array('success'=>$this->success, 'data'=>$this->data);
	}

	public function printJson(){
		header('Content-Type: application/json');
		echo json_encode($this->response);

	}




}
