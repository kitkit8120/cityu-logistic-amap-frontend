<?php

class user_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "users";
        $this->load->database();
    }

    public function getUsers()
	{
        $query = $this->db->get($this->table);
        return $query->result();
	}

	public function insert($data) {
	    $this->db->insert($this->table, $data);
	    $last_id = $this->db->insert_id();
	    return $last_id;
	}

	public function getByUsername($username) {
	    $query = $this->db->get_where($this->table, array('username' => $username));
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
		      return $row;
		   }
		}else{
			return false;
		}
	}

	public function login($username, $password){
		$query = $this->db->get_where($this->table, array('username' => $username , 'password' => $password, 'type' => 0));
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
		      return $row;
		   }
		}else{
			return false;
		}

	}

	public function loginToDriver($username, $password){
		$query = $this->db->get_where($this->table, array('username' => $username , 'password' => $password, 'type' => 1));
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				return $row;
			}
		}else{
			return false;
		}

	}

}
