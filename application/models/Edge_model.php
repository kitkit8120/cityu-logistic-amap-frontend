<?php
class edge_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "road_link";
        $this->load->database();
    }

    public function getEdgeByNodeId($oId, $dId)
	{

		$this->db->select('road_link.id, distance AS OSMLength , name, original_nodeId,destination_nodeId ,origin.lat as OSM_origin_lat,
		 origin.lng as OSM_origin_lng, destination.lat as OSM_dest_lat, destination.lng as OSM_dest_lng ');
//		$this->db->select('road_link.id');
		$this->db->from('road_link');
		$this->db->join('road_node as origin', 'road_link.original_nodeId = origin.nodeId');
		$this->db->join('road_node AS destination', 'road_link.destination_nodeId = destination.nodeId');
		$this->db->where('original_nodeId', $oId);
		$this->db->where('destination_nodeId', $dId);
		$query = $this->db->get();
		return $query->first_row();
	}

	public function getEdgePathByEdgeId($edgeId)
	{

		$this->db->select('*');
		$this->db->from("road_link_path");
		$this->db->where('id', $edgeId);
		$this->db->order_by("sequence", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllEdge(){
		$this->db->select('*');
		$this->db->from($this->table);
//		$this->db->order_by("sequence", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function getEdgePathByEdgeIdIn($array)
	{

		$this->db->select('*');
		$this->db->from("road_link_path");
		$this->db->where_in('id', $array);
		$this->db->order_by("id", "asc");
		$this->db->order_by("sequence", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function mutlipleQuery($nodes){
		$this->db->trans_start();

		$this->db->query('AN SQL QUERY...');
		$this->db->query('ANOTHER QUERY...');
		$this->db->query('AND YET ANOTHER QUERY...');
		$this->db->trans_complete();
	}








	



}
