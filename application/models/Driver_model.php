<?php
class driver_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "drivers";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllValid()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('status', True);
		$this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		return $query;
	}

	public function getByDriverId($driverId){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('driverId', $driverId);
		$this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
		      return $row;
		   }
		}
	}

	public function insert( $data) {
	    $this->db->insert($this->table, $data);
	}

	public function updateStatus($driverId, $status) {
		$this->db->set('status', $status);
	    $this->db->where('driverId', $driverId);
	    $this->db->update($this->table);
	}

	public function update($driverId, $data) {
	    $this->db->where('driverId', $driverId);
	    $this->db->update($this->table, $data);
	}

	public function delete($driverId) {
	    $this->db->where('driverId', $driverId);
	    $this->db->delete($this->table);
	}

	public function getByUserId($userId){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('userId', $userId);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				return $row;
			}
		}
	}




}
