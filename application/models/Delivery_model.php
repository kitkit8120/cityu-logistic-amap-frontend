<?php
class delivery_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "deliveries";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('drivers', 'deliveries.driverId = drivers.driverId');
		$this->db->join('orders', 'deliveries.orderId = orders.orderId',"left");
		$this->db->join('vehicles', 'deliveries.vehicleId = vehicles.vehicleId');
		$this->db->join('statuses', 'deliveries.deliveryStatusId = statuses.statusId',"left");
		$query = $this->db->get();
		return $query->result();
	}
	


	public function insert( $data) {
	    $this->db->insert($this->table, $data);
	    $insert_id = $this->db->insert_id();
	    return  $insert_id;
	}

	public function replace( $data) {
		$this->db->replace($this->table, $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function update($driverId, $data) {
	    $this->db->where('driverId', $driverId);
	    $this->db->update($this->table, $data);
	}

	public function getDeliveryByOrderId($orderId){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('orderId', $orderId);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->first_row();
	}

	public function updateDeliveryByDeliveryId($data,$deliveryId){
		$this->db->where('deliveryId', $deliveryId);
		$this->db->update($this->table,$data);

	}

	public function getAllDeliveriesByDate($date){
		$this->db->select('* , deliveries.orderId as orderId');
		
		$this->db->where('deliveries.deliveryCalDate', $date);
		$this->db->join('drivers', 'deliveries.driverId = drivers.driverId');
		$this->db->join('orders', 'deliveries.orderId = orders.orderId',"left");
		$this->db->join('vehicles', 'deliveries.vehicleId = vehicles.vehicleId');
		$this->db->join('statuses', 'deliveries.deliveryStatusId = statuses.statusId',"left");
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function deleteByDeliveryId($deliveryId){
		$this->db->delete($this->table, array('deliveryId' => $deliveryId));
	}

	public function getDriverDeliveriesByDate($date){
		$this->db->distinct();
		$this->db->select('driverId');
		$this->db->from($this->table);
		$this->db->where('deliveries.deliveryCalDate', $date);
		$query = $this->db->get();
		return $query->result();
	}

	public function getDeliveriesByDriverIdAndDate($driverId,$date){

		$this->db->select('*, orders.capacity as order_capacity, deliveries.createDate as generationDate, deliveries.orderId as orderId');
		$this->db->where('deliveries.deliveryCalDate', $date);
		$this->db->where('deliveries.driverId', $driverId);
		$this->db->join('drivers', 'deliveries.driverId = drivers.driverId');
		$this->db->join('orders', 'deliveries.orderId = orders.orderId',"left");
		$this->db->join('vehicles', 'deliveries.vehicleId = vehicles.vehicleId');
		$this->db->join('statuses', 'deliveries.deliveryStatusId = statuses.statusId',"left");
		$this->db->from($this->table);
		$this->db->order_by('deliveries.vehicleId asc, deliveryTime asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function getReturnDeliveryByDriverIdAndVehicleIdAndDeliveryDate($driverId,$vehicleId,$deliveryDate){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('driverId', $driverId);
		$this->db->where('vehicleId', $vehicleId);
		$this->db->where('deliveryCalDate', $deliveryDate);
		$this->db->limit(1);
		$this->db->order_by('deliveryTime desc');
		$query = $this->db->get();
		return $query->first_row();
	}

	public function updateStatusId($orderId, $statusId) {
		$this->db->set('deliveryStatusId', $statusId);
		$this->db->where('orderId', $orderId);
		$this->db->update($this->table);
	}


}
