<?php
class cpd_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "cpd";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function insert($data) {
	    $this->db->insert($this->table,$data);
	}

	public function insert_batch($data) {
	    $this->db->insert_batch($this->table,$data);
	}

	public function getLatestDate(){
		$this->db->distinct();
		$this->db->select('targetDate');
		$this->db->from($this->table);
		$this->db->order_by('targetDate', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->first_row();
	}

	public function getCPDByNodeIdAndTargetDateAndToIndex($nodeId, $targetDate,$toIndex){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('nodeId', $nodeId);
		$this->db->where('targetDate', $targetDate);
		$this->db->where('firstMoveIndex <=', $toIndex);
		
		$this->db->order_by('firstMoveIndex', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->first_row();
	}

	



}