<?php
class order_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "orders";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('statuses', 'orders.statusId = statuses.statusId');
		$query = $this->db->get();
		return $query->result();
	}

	public function getDynamicOrdersByDate($date)
	{
		$statusIds = array(2,3,4);
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deliveryDate', $date);
		$this->db->where_in('orders.statusId', $statusIds);
		$this->db->join('statuses', 'orders.statusId = statuses.statusId');
		// $this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		return $query->result();
	}

	public function getOrdersByDate($date)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deliveryDate', $date);
		$this->db->join('statuses', 'orders.statusId = statuses.statusId');
		// $this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		return $query->result();
	}

	public function getByOrderId($driverId){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('orderId', $driverId);
		$this->db->join('statuses', 'orders.statusId = statuses.statusId');
		// $this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
		   foreach ($query->result() as $row)
		   {
		      return $row;
		   }
		}
	}

	public function getByDeliveryDate($deliveryDate){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deliveryDate', $deliveryDate);
		$this->db->join('statuses', 'orders.statusId = statuses.statusId');
		// $this->db->join('users', 'users.userId = drivers.userId');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	public function getNonPlannedOrdersByDate($deliveryDate){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deliveryDate', $deliveryDate);
		$this->db->where('orders.statusId', 3);
		$this->db->join('statuses', 'orders.statusId = statuses.statusId');
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}



	public function insert( $data) {
	    $this->db->insert($this->table, $data);
	}

	public function update($driverId, $data) {
	    $this->db->where('orderId', $driverId);
	    $this->db->update($this->table, $data);
	}

	public function delete($driverId) {
	    $this->db->where('orderId', $driverId);
	    $this->db->delete($this->table);
	}

	public function updateStatusId($orderId, $statusId) {
		$this->db->set('statusId', $statusId);
	    $this->db->where('orderId', $orderId);
	    $this->db->update($this->table);
	}





}
