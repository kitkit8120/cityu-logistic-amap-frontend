<?php
class depot_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "depots";
        $this->load->database();
    }

    public function get()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->first_row();
	}


	



}