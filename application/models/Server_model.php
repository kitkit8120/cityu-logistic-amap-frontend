<?php
class server_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "servers";
        $this->load->database();
    }

    public function getAmapKeyIdByIp($ip)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('server_amap_key', 'server_amap_key.serverId = servers.Id');
		$this->db->where('address', $ip);
		$query = $this->db->get();
		return $query->result();
	}

	public function getServerIdByIp($ip)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('address', $ip);
		$query = $this->db->get();
		return $query->first_row();
	}



	public function insert($data) {
	    $this->db->insert($this->table,$data);
	}





}
