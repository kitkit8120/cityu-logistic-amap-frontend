<?php

class delivery_route_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "delivery_routes";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function insert($data) {
	    $this->db->insert($this->table,$data);
	}

	public function insert_batch($data) {
	    $this->db->insert_batch($this->table,$data);
	}

	public function getDeliveryRouteByDeliveryId($deliveryId){
		$this->db->select('*');
		$this->db->where("deliveryId",$deliveryId);
		$this->db->from($this->table);
		$this->db->order_by('sequence', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function getCountByDeliveryId($deliveryId){
		$this->db->select('COUNT(*) as myCount');
		$this->db->from($this->table);
		$this->db->where("deliveryId",$deliveryId);
		$query = $this->db->get();
		return intval($query->first_row()->myCount);
	}

	public function deleteByDeliveryId($deliveryId){
		$this->db->where("deliveryId",$deliveryId);
		$this->db->delete($this->table);
	}

	



}
