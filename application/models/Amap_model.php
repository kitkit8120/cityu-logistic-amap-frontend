<?php
class amap_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "amap_adjust_road_link";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function insert($data) {
	    $this->db->insert($this->table,$data);
	}

	public function insert_batch($data) {
	    $this->db->insert_batch($this->table,$data);
	}

	public function replaceAdjusted($data){
		$this->db->replace($this->table,$data);
	}

	public function replaceManualAdjusted($data){
		$this->db->replace('amap_manual_adjust_road_link',$data);
	}

	public function replaceRoadLink($data){
		$this->db->replace('amap_road_link',$data);
	}

	public function manualAdjustUpdate($id,$status){
		$this->db->set('manualAdjusted', $status);
		$this->db->where('id', $id);
		$this->db->update($this->table);
	}

	public function getProblemRoadSet($targetTime){

		$sql = "SELECT * FROM amap_pending_manual_adjust_road_link where updateDate = '".$targetTime."'";
		$query = $this->db->query($sql);
		return $query->result();
	}


	public function getProblemManualRoadSetById($requiredId){


		$sql = "SELECT road_link.id, distance as OSMLength ,amap_manual_adjust_road_link.length AS amapLength,
				origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng,
				road_link.name as OSMName, amap_manual_adjust_road_link.name AS amapName, amap_manual_adjust_road_link.updateDate as updateDate,
				amap_manual_adjust_road_link.origin_lat as amap_origin_lat, amap_manual_adjust_road_link.origin_lng as amap_origin_lng,
				amap_manual_adjust_road_link.dest_lat as amap_dest_lat, amap_manual_adjust_road_link.dest_lng as amap_dest_lng
				FROM road_link ,road_node as origin, road_node as destination ,amap_manual_adjust_road_link
				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId
				AND amap_manual_adjust_road_link.id = road_link.id AND
                road_link.id IN (".implode(',',$requiredId).")
                ";
		$query = $this->db->query($sql);

		return $query->result();

	}

	public function getProblemAPIAdjustRoadSetById($requiredIdSet, $notRequiredIdSet){


		$sql = "SELECT road_link.id, distance as OSMLength ,amap_adjust_road_link.length AS amapLength,
				origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng,
				road_link.name as OSMName,
				amap_adjust_road_link.origin_lat as amap_origin_lat, amap_adjust_road_link.origin_lng as amap_origin_lng,
				amap_adjust_road_link.dest_lat as amap_dest_lat, amap_adjust_road_link.dest_lng as amap_dest_lng
				FROM road_link ,road_node as origin, road_node as destination ,amap_adjust_road_link
				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId
				AND amap_adjust_road_link.id = road_link.id AND
                road_link.id IN (".implode(',',$requiredIdSet).")";

		if(sizeof($notRequiredIdSet)>0){
			$sql .="AND
                road_link.id NOT IN (".implode(',',$notRequiredIdSet).")";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}

	public function getProblemAPIAdjustRoadSetByIdLimit50($requiredIdSet, $notRequiredIdSet){


		$sql = "SELECT road_link.id, distance as OSMLength ,amap_adjust_road_link.length AS amapLength,
				origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng,
				road_link.name as OSMName,
				amap_adjust_road_link.origin_lat as amap_origin_lat, amap_adjust_road_link.origin_lng as amap_origin_lng,
				amap_adjust_road_link.dest_lat as amap_dest_lat, amap_adjust_road_link.dest_lng as amap_dest_lng
				FROM road_link ,road_node as origin, road_node as destination ,amap_adjust_road_link
				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId
				AND amap_adjust_road_link.id = road_link.id AND
                road_link.id IN (".implode(',',$requiredIdSet).")";

		if(sizeof($notRequiredIdSet)>0){
			$sql .="AND
                road_link.id NOT IN (".implode(',',$notRequiredIdSet).")";
		}
		$sql .= " LIMIT 50";
		$query = $this->db->query($sql);
		return $query->result();

	}

	public function getNoManualAdjustRoadSetWithUpdateTime(){

//    	$sql = "SELECT road_link.id, distance as OSMLength,  name as OSMName,
//				origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng,
//				amap_origin.lat as amap_origin_lat, amap_origin.lng as amap_origin_lng, amap_dest.lat as amap_dest_lat, amap_dest.lng as amap_dest_lng
//				FROM road_link ,road_node as origin, road_node as destination,
//				amap_node as amap_origin, amap_node as amap_dest
//				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId
//				AND road_link.original_nodeId = amap_origin.id AND road_link.destination_nodeId = amap_dest.id
//				AND road_link.id not in (select id from amap_adjust_road_link)
//				AND road_link.id not in (select  id from amap_manual_adjust_road_link) limit 100";
//		$sql = "SELECT road_link.id, distance as OSMLength ,amap_road_link.amapLength as amapLength,
//				origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng,
//				name as OSMName, amapName,
//
//				amap_adjust_road_link.origin_lat as amap_origin_lat, amap_adjust_road_link.origin_lng as amap_origin_lng,
//				amap_adjust_road_link.dest_lat as amap_dest_lat, amap_adjust_road_link.dest_lng as amap_dest_lng
//
//				FROM road_link ,road_node as origin, road_node as destination, amap_road_link ,amap_adjust_road_link, amap_osm_compare_type
//				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId
//				AND road_link.id = amap_road_link.id AND amap_adjust_road_link.id = road_link.id
//				AND amap_osm_compare_type.id = road_link.id  AND diffLength >5
//                AND amap_osm_compare_type.type !=1 AND road_link.id NOT IN (SELECT id FROM amap_manual_adjust_road_link)
//
//               	UNION
//				SELECT road_link.id, distance as OSMLength ,amap_road_link.amapLength as amapLength,
//				origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng,
//				name as OSMName, amapName,
//
//				amap_adjust_road_link.origin_lat as amap_origin_lat, amap_adjust_road_link.origin_lng as amap_origin_lng,
//				amap_adjust_road_link.dest_lat as amap_dest_lat, amap_adjust_road_link.dest_lng as amap_dest_lng
//
//				FROM road_link ,road_node as origin, road_node as destination, amap_road_link ,amap_adjust_road_link, amap_osm_compare_type
//				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId
//				AND road_link.id = amap_road_link.id AND amap_adjust_road_link.id = road_link.id
//				AND amap_osm_compare_type.id = road_link.id AND diffLength >10
//                AND amap_osm_compare_type.type =1 AND road_link.id NOT IN (SELECT id FROM amap_manual_adjust_road_link)  LIMIT 100";


	}

	public function getManualAdjustRoadSet($start,$end){
//
		$sql = "SELECT * from (SELECT road_link.id, distance as OSMLength, road_link.name as OSMName, amap_road_link.amapLength as amapLength, amap_road_link.amapName as amapName , origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng, amap_adjust_road_link.origin_lat as amap_origin_lat, amap_adjust_road_link.origin_lng as amap_origin_lng, amap_adjust_road_link.dest_lat as amap_dest_lat, amap_adjust_road_link.dest_lng as amap_dest_lng FROM road_link, amap_road_link,amap_adjust_road_link, road_node as origin, road_node as destination 
				WHERE road_link.id not in (SELECT id FROM amap_manual_adjust_road_link) AND
				amap_road_link.id = road_link.id AND
				amap_adjust_road_link.id = road_link.id AND
				road_link.original_nodeId = origin.nodeId AND
				road_link.destination_nodeId = destination.nodeId
				UNION
				SELECT road_link.id, distance as OSMLength, road_link.name as OSMName, amap_road_link.amapLength as amapLength, amap_road_link.amapName as amapName , origin.lat as origin_lat, origin.lng as origin_lng, destination.lat as dest_lat, destination.lng as dest_lng, amap_manual_adjust_road_link.origin_lat as amap_origin_lat, amap_manual_adjust_road_link.origin_lng as amap_origin_lng, amap_manual_adjust_road_link.dest_lat as amap_dest_lat, amap_manual_adjust_road_link.dest_lng as amap_dest_lng FROM road_link, amap_road_link,amap_manual_adjust_road_link, road_node as origin, road_node as destination 
				WHERE amap_manual_adjust_road_link.exist=1 AND
				amap_road_link.id = road_link.id AND
				amap_manual_adjust_road_link.id = road_link.id AND
				road_link.original_nodeId = origin.nodeId AND
				road_link.destination_nodeId = destination.nodeId) info Limit $start,$end";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getCountOfAmapRoads(){
		$this->db->select('count(*)');
		$this->db->from("amap_road_link");
//		$this->db->where('id', $id);
//		$this->db->order_by("createDate", "desc");r
		$query = $this->db->get();
		$cnt = $query->row_array();
		return $cnt['count(*)'];
	}

	public function getCountRecordOfAmapRoadId($id){
		$this->db->select('count(*)');
		$this->db->from("road_record");
		$this->db->where('id', $id);
//		$this->db->where('id', $id);
//		$this->db->order_by("createDate", "desc");r
		$query = $this->db->get();
		$cnt = $query->row_array();
		return $cnt['count(*)'];
	}

	public function getRecordsByIdWithLimit($id,$start,$count){
//

		$this->db->select('*');
		$this->db->from("road_record");
		$this->db->where('id', $id);
		$this->db->order_by("createDate", "desc");
		$this->db->limit($start,$count);
		$query = $this->db->get();

		return $query->result();
	}




	public function getPathByRoadId($id){
		$this->db->select('*');
		$this->db->from("road_link_path");
		$this->db->where('id', $id);
		$this->db->order_by("sequence", "asc");
		$query = $this->db->get();

		return $query->result();
	}

	public function checkManualAdjust($id){
		$this->db->select('*');
		$this->db->from('amap_manual_adjust_road_link');
		$this->db->where('id', $id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() >0)
			return true;
		else
			return false;
	}


	public function checkRoadHasAdjust($id){
		$this->db->select('*');
		$this->db->from('amap_adjust_road_link');
		$this->db->where('id', $id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() >0)
			return true;
		else
			return false;
	}

	public function  getAdjustRoadDetailFromManuallyById($id){
		$sql = "SELECT road_link.id, distance as OSMLength,  road_link.name as OSMName, amap.name as amapName, amap.length as amapLength,
				origin.lat as OSM_origin_lat, origin.lng as OSM_origin_lng, destination.lat as OSM_dest_lat, destination.lng as OSM_dest_lng,
				amap.origin_lat as amap_origin_lat, amap.origin_lng as amap_origin_lng, amap.dest_lat as amap_dest_lat, amap.dest_lng as amap_dest_lng
				FROM road_link ,road_node as origin, road_node as destination, amap_manual_adjust_road_link as amap
				WHERE road_link.original_nodeId = origin.nodeId AND road_link.destination_nodeId = destination.nodeId 
                AND amap.id = road_link.id
				AND road_link.id = {$id}";
		$query = $this->db->query($sql);
		return $query->first_row();
	}

	public function getHaveAdjustRoadDetailById($id){
		$this->db->select('road_link.id, distance AS OSMLength , name as OSMName,  amapLength , amapName as amapName, origin.lat as OSM_origin_lat, 
		 origin.lng as OSM_origin_lng, destination.lat as OSM_dest_lat, destination.lng as OSM_dest_lng, amap_adjust_road_link.origin_lat as amap_origin_lat,
		  amap_adjust_road_link.origin_lng as amap_origin_lng,amap_adjust_road_link.dest_lat as amap_dest_lat,amap_adjust_road_link.dest_lng as amap_dest_lng');
		$this->db->from('road_link');
		$this->db->join('road_node as origin', 'road_link.original_nodeId = origin.nodeId');
		$this->db->join('road_node AS destination', 'road_link.destination_nodeId = destination.nodeId');
		$this->db->join('amap_adjust_road_link', 'road_link.id = amap_adjust_road_link.id');
		$this->db->join('amap_road_link', 'road_link.id = amap_road_link.id');
		$this->db->where('road_link.id', $id);
		$query = $this->db->get();
		return $query->first_row();
	}

	public function getLatestComparedUpdateTime(){
		$this->db->select("updateDate");
		$this->db->from('amap_pending_manual_adjust_road_link');
		$this->db->order_by("updateDate", "desc");
		$query = $this->db->get();
		return $query->first_row();
	}

	public function getNoAdjustRoadDetailById($id)
	{
		$this->db->select('road_link.id, distance AS OSMLength , name as OSMName, origin.lat as OSM_origin_lat, 
		 origin.lng as OSM_origin_lng, destination.lat as OSM_dest_lat, destination.lng as OSM_dest_lng, amap_origin.lat as amap_origin_lat,amap_origin.lng as amap_origin_lng, amap_dest.lat as amap_dest_lat, amap_dest.lng as amap_dest_lng');
		$this->db->from('road_link');
		$this->db->join('road_node as origin', 'road_link.original_nodeId = origin.nodeId');
		$this->db->join('road_node AS destination', 'road_link.destination_nodeId = destination.nodeId');
		$this->db->join('amap_node AS amap_origin', 'road_link.original_nodeId = amap_origin.id');
		$this->db->join('amap_node AS amap_dest', 'road_link.destination_nodeId = amap_dest.id');
		$this->db->where('road_link.id', $id);
		$query = $this->db->get();
		return $query->first_row();
	}

	public function getAmapRequestRoadByServerId($serverId){
    	$sql = "SELECT * FROM (SELECT id,origin_lat,origin_lng,dest_lat,dest_lng 
FROM amap_adjust_road_link 
WHERE id not in (SELECT id FROM amap_manual_adjust_road_link) UNION SELECT id,origin_lat,origin_lng,dest_lat,dest_lng FROM amap_manual_adjust_road_link WHERE exist = 1 ) r WHERE id in (Select roadId as id in road_server WHERE serverId = {$serverId})";
		$query = $this->db->query($sql);
		return $query->result();
	}

	



}
