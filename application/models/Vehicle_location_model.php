<?php

class vehicle_location_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "vehicles_location";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function insert($data) {
	    $this->db->insert($this->table,$data);
	}

	public function getLatestLocationByVehicleId($vehicleId){
		$this->db->select('vehicles_location.vehicleId, latitude, longitude, vehicles_location.createDate, vehicles.vehicleCode');
		$this->db->where("vehicles_location.vehicleId",$vehicleId);
		$this->db->from($this->table);
		$this->db->join('vehicles', 'vehicles_location.vehicleId = vehicles.vehicleId');
		$this->db->order_by('vehicles_location.createDate', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
		// print_r($this->db->last_query());    
		return $query->first_row();
	}

}