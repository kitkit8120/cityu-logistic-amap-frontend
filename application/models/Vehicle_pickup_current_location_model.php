<?php

class vehicle_pickup_current_location_model extends CI_Model {

    public function __construct()
    {

    	$this->table = "vehicle_pickup_current_location";
        $this->load->database();
    }

    public function getAll()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function insert($data) {
	    $this->db->insert($this->table,$data);
	}

	public function getByDeliveryId($deliveryId){
		$this->db->select('*');
		$this->db->where("deliveryId",$deliveryId);
		$this->db->from($this->table);
		$this->db->limit(1);
		$query = $this->db->get();
		// print_r($this->db->last_query());    
		return $query->first_row();
	}

}
