
<script type='text/javascript'>
<?php
$js_array = json_encode($totalDriverDeliveies);
echo "var totalDriverDeliveies = ". $js_array . ";\n";

$js_array = json_encode($vehilcesLocation);
echo "var vehilcesLocation = ". $js_array . ";\n";

?>
</script>



<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Places Search Box</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      .pac-container {
        z-index: 100000;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
    </style>

  </head>
  <body>
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">

            </div>
            

            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 modal_body_map">
                  <div class="location-map" id="location-map">
                    <div id="map" style="width: 100%; height: 300px;"></div>
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-md-12 modal_body_end">
                  <div class="row">
                    <div class="col-4">
                    	<select id="selectionCar" class="custom-select mt-4 pr-4 pl-4">
					  		<option value="-1" selected>All routes</option>

					  		<?php 
					  			$k =0;
					  			foreach($totalDriverDeliveies as $car){
                    $name = $car[0]->firstName . " " .$car[0]->lastName;
                    $vehicleCode = $car[0]->vehicleCode;
					  				echo "<option value='{$k}'>{$vehicleCode} - {$name} </option>";
                    $k+=1;
					  			}


					  		?>
						</select>
                    </div>

                    <div class="col-4  mt-4 pr-4 pl-4">
<!--						<input type="checkbox" checked data-toggle="toggle" data-on="Hello<br>World" data-off="Goodbye<br>World">-->
<!--						<input id="toggle-vehicle"  data-on="<i class='fa fa-play'></i> Play" data-off="<i class='fa fa-pause'></i> Pause"  />-->
                    </div>
                    <div class="col-4"><button id='btnReload' style=" width: inherit; background-color: #269A9A" class="btn btn-primary mt-4 pr-4 pl-4">Reload</button>
					</div>
                  </div>
                </div>
              </div>
              
            </div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7uNvqr1TVubvcyG9Ic-9Yi-wBlonH45Q&callback=initialize" async defer></script>
            <script>
	      var map;
	      var carList = totalDriverDeliveies;

	      var markers = []; // Create a marker array to hold your markers
        var googleLines = [];

        function combineCarListSamePoint(locations) {
            var newLocations = []
			for(var i = 0; i< locations.length;i++){
                var delivery = locations[i];
                var add = false
				for(var k=0 ; k<newLocations.length;k++){
                    element = newLocations[k]
					if(element["latitude"] == delivery["latitude"] && element["longitude"] == delivery["longitude"]){
					    if(element["orderId"]=="0"){
                            element["orderId"]="Depot"
						}else{
                            element["orderId"] = element["orderId"] +" & " + delivery["orderId"];
						}
                        element["deliveryTime"] = element["deliveryTime"] + " & " + delivery["deliveryTime"]
                        newPath = element["deliveryRoute"].concat(delivery["deliveryRoute"]);
                        element["deliveryRoute"] = newPath;
                        add = true
                        // console.log(element)
                        newLocations[k] = element
                    }
				}

                if(add==false){
                    newLocations.push(delivery)
				}
			}
			// console.log(newLocations)
			return newLocations;

        }
		  
		function setMarkers(locations, vehicleIndex) {
			var infowindow = new google.maps.InfoWindow();
		    for (var i = 1; i < locations.length; i++) {
  		    	// if(i==0){
  		    	// 	continue;
  		    	// }
				var index = i;
		        var delivery = locations[i];
		        if(delivery["orderId"]=="-1"){
		            continue;
				}
		        var myLatLng = new google.maps.LatLng(delivery["latitude"], delivery["longitude"]);
		        var marker = new google.maps.Marker({
		            position: myLatLng,
		            map: map,
		            animation: google.maps.Animation.DROP,
		            label:{
	                  text: "Sequence("+index+")",
	                  color: 'green',
	                  fontSize: "18px",
	                  fontWeight : 'bold'
	                },
	                icon: {
	                url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
	                },
		        });

            for (var k= 0; k < delivery["deliveryRoute"].length; k ++) {
                var line = [];
                targetline =  delivery["deliveryRoute"][k];
                for (var g= 0; g < targetline.length; g +=2){
                  temp = {lat : parseFloat(targetline[g]) , lng : parseFloat(targetline[g+1])};
                  line.push(temp)
                }
                var lineSymbol = {
                  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                };
                googleLine = new google.maps.Polyline({
                icons: [{
                  icon: lineSymbol,
                  offset: '100%'
                }],
                path: line,
                strokeColor: '#0000FF',
                strokeOpacity: 0.5,
                strokeWeight: 1
                });
                googleLines.push(googleLine);
            }
		        google.maps.event.addListener(marker, 'click', (function(marker, i) {
				    return function() {
				    	var delivery = locations[i];
                        var content= "<p>" + "Sequence : " + i +"</p>"
                        content+= "<p>" + "OrderId : " + delivery['orderId'] +"</p>"
				    	content += "<p>" +"Address : " +  delivery["address"] +"</p>"
				    	content+= "<p>" + "Driver : " + delivery["firstName"] +" "+ delivery["lastName"] +"</p>"
				    	content+= "<p>" + "Vehicle : " + delivery["vehicleCode"] +"</p>"


		        	content+= "<p>" + "DeliveryTime : " +  delivery["deliveryTime"]+"</p>"
				      infowindow.setContent(content);
				      infowindow.open(map, marker);
				    }
				  })(marker, i));
		        
		        // Push marker to markers array
		        markers.push(marker);
		    }
            markers.push(getDepotMarker(locations[0], locations.length));


            markers.push(getVehicleMarker(vehicleIndex));


			  // console.log(googleLines)
			  addLine();
		}

		function getDepotMarker(delivery, length) {
            var infowindow = new google.maps.InfoWindow();
			var myLatLng = new google.maps.LatLng(delivery["latitude"], delivery["longitude"]);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				animation: google.maps.Animation.DROP,
				label:{
					text: "Depot(" +length+")",
					color: 'blue',
					fontSize: "18px",
					fontWeight : 'bold'
				},
				icon: {
					url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
				},
			});

			for (var k= 0; k < delivery["deliveryRoute"].length; k ++) {
				var line = [];
				targetline =  delivery["deliveryRoute"][k];
				for (var g= 0; g < targetline.length; g +=2){
					temp = {lat : parseFloat(targetline[g]) , lng : parseFloat(targetline[g+1])};
					line.push(temp)
				}
				var lineSymbol = {
					path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
				};
				googleLine = new google.maps.Polyline({
					icons: [{
						icon: lineSymbol,
						offset: '100%'
					}],
					path: line,
					strokeColor: '#0000FF',
					strokeOpacity: 0.5,
					strokeWeight: 1
				});
				googleLines.push(googleLine);
			}

			marker.addListener('click',function(){
				return function() {
					var content=  "<p>" +"Address : " +  delivery["address"] +"</p>"
					content+= "<p>" + "Driver : " + delivery["firstName"] +" "+ delivery["lastName"] +"</p>"
					content+= "<p>" + "Vehicle : " + delivery["vehicleCode"] +"</p>"
					content+= "<p>" + "DeliveryTime : " +  delivery["deliveryTime"]+"</p>"
					infowindow.setContent(content);
					infowindow.open(map, marker);
				}
			});
			// Push marker to markers array
			return marker;
		}


    function addLine(){
      for(k=0;k<googleLines.length;k++){
        path = googleLines[k];
        path.setMap(map);
      }
    }

    function removeLine() {
      for(k=0;k<googleLines.length;k++){
        path = googleLines[k];
        path.setMap(null);
      }
      googleLines = [];
    }

		function reloadMap() {
 
    	// Loop through markers and set map to null for each
		    for (var i=0; i<markers.length; i++) {
		     
		        markers[i].setMap(null);
		    }
        	removeLine();

		    // for (var i=0; i<flightPaths.length; i++) {
		     
		    //     flightPaths[i].setMap(null);
		    // }
		    
		    // Reset the markers array
		    markers = [];
		    // flightPaths = [];
       
			e = document.getElementById('selectionCar')
				value = e.options[e.selectedIndex].value;
			if(value ==-1){
			  setAllMarkers()
			}else{
			    combinedCarList = combineCarListSamePoint(carList[parseInt(value)])
			  setMarkers(combinedCarList, parseInt(value));
			}
				// Call set markers to re-add markers

				// setPath();
		}

      function getVehicleMarker(count){
		  	var markerId = "ID" + count;
			var infowindow = new google.maps.InfoWindow();

			var vehicleLocation = vehilcesLocation[count];
			var myLatLng = new google.maps.LatLng(vehicleLocation["latitude"], vehicleLocation["longitude"]);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				animation: google.maps.Animation.DROP,
				label:{
					text: ""+vehicleLocation['vehicleCode'],
					color: 'black',
					fontSize: "16px",
					fontWeight : 'bold'
				  },
				  icon: {
				  url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
				  },
			});

			google.maps.event.addListener(marker, 'click', (function(marker, markerId) {
			  return function() {
				var vehicleLocation = vehilcesLocation[count];
				var content = "<p>" +"vehicleCode : " +  vehicleLocation["vehicleCode"] +"</p>"
				content+= "<p>" + "vehicle update time : " + vehicleLocation["createDate"] +"</p>"
				infowindow.setContent(content);
				infowindow.open(map, marker);
			  }
			})(marker, count));

			return marker

      }

  		function setAllMarkers(){
  			for(var i=0; i<carList.length; i++){
                combinedCarList = combineCarListSamePoint(carList[i])
  				setMarkers(combinedCarList, i);
  			}
  		}

	      

	      function initialize() {
    
		    var mapOptions = { center: new google.maps.LatLng(22.363154, 114.131202), zoom: 13,
	        mapTypeId: google.maps.MapTypeId.ROADMAP };
		    
		    map = new google.maps.Map(document.getElementById('map'), mapOptions);
		    setAllMarkers()
        	// setVehicleMarkers()
		    // Bind event listener on button to reload markers
		    document.getElementById('btnReload').addEventListener('click', reloadMap);
		}

		

	     
			</script>
            
            </div>
            <div class="modal-footer">
              <div class="row">
                  
            </div>
          </div>
        </div>
    </div>
  </body>
</html>
