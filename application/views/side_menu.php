<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="index.php"><p class='h4 text-white'>Logistic System</p> </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">

                    <li class="active">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-dashboard"></i><span>Amap dashboard</span></a>
                        <ul class="collapse">
                            <li><a href="<?php echo base_url(); ?>index.php/amap/view">List Amap Road Link</a></li>
							<li><a href="<?php echo base_url(); ?>index.php/amap/viewProblemsRoad">List Problem Road Link</a></li>
                        </ul>
                    </li>

                </ul>
            </nav>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#targetGenerateDate').datetimepicker({format:'YYYY-MM-DD'});
    $('#targetDate').datetimepicker({format:'YYYY-MM-DD'});


})
</script>

<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="generateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="form-group">
            <form method="POST" action="<?php echo base_url()?>index.php/deliveries/generateDelivery">
              <div class="modal-header">
                <h5 class="modal-title" id="generateModalLabel">Generate Delivery List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <label class="col-form-label">Target Generate Date</label>
                        <div class="form-group">
                           <div class="input-group date" id="targetGenerateDate" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#targetGenerateDate" name="targetGenerateDate" />
                                <div class="input-group-append" data-target="#targetGenerateDate" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Confirm</button>
              </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="targetDateModal" tabindex="-1" role="dialog" aria-labelledby="targetDateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="form-group">
            <form method="POST" action="<?php echo base_url()?>index.php/deliveries/viewByDate">
              <div class="modal-header">
                <h5 class="modal-title" id="targetDateModalLabel">Get Delivery List by Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <label class="col-form-label">Target Date</label>
                        <div class="form-group">
                           <div class="input-group date" id="targetDate" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" data-target="#targetDate" name="targetDate" />
                                <div class="input-group-append" data-target="#targetDate" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Confirm</button>
              </div>
            </form>
        </div>
    </div>
  </div>
</div>
