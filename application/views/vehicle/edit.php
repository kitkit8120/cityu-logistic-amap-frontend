<!doctype html>
<html class="no-js" lang="en">

<head>
    
    <script type="text/javascript">

	</script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="view">Home</a></li>
                                <li><span>Edit a Vehicle</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Edit a Vehicle</h4>
                                        <div class="form-group">
                                            <form id='edit_from'method="POST" action="<?php echo base_url()?>index.php/vehicles/editSubmit">
                                                <?php echo "<input type='hidden' name='vehicleId' value='{$vehicle->vehicleId}'/>"?>
                                             
                                            	<div class="row">
    	                                        	<div class="col-6">
    	                                        		<label for="vehicleCode" class="col-form-label">Vehicle Code</label>
    	                                            	<input class="form-control" type="text" value="<?php echo $vehicle->vehicleCode;?>" name='vehicleCode' id="vehicleCode">
    	                                        	</div>
                                                    <div class="col-6">
                                                        <label for="capacity" class="col-form-label">Vehicle Capacity</label>
                                                        <input class="form-control" type="number" value="<?php echo $vehicle->capacity;?>" name='capacity' id="capacity">
                                                    </div>

                                            	</div>
                                            	<div class="row">
                                            		<div class="col-5"></div>
                                            		<div class="col-4">
                                            			
                                            		</div>
                                            		<div class="col-3"><button style=" width: inherit; background-color: #269A9A" type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
                                                    </div>
                                            	</div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
</body>

</html>
