<!doctype html>
<html class="no-js" lang="en">

<head>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
	<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
	<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=dc6fad3566b2537ea9b13f419004bc14&plugin=AMap.Driving"></script>
	<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
	<script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v6.1.1/build/ol.js"></script>
	<link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v6.1.1/css/ol.css">

    <script type="text/javascript">
		<?php $paths = json_encode($paths);
		echo "var paths = ". $paths . ";\n";

		?>
        var amap_origin_lat = <?php echo $road->amap_origin_lat ?>;
        var amap_origin_lng = <?php echo $road->amap_origin_lng ?>;
        var amap_dest_lat = <?php echo $road->amap_dest_lat ?>;
        var amap_dest_lng = <?php echo $road->amap_dest_lng ?>;
        var origin_lat = <?php echo $road->OSM_origin_lat; ?>;
        var origin_lng = <?php echo $road->OSM_origin_lng ;?>;
        var dest_lat = <?php echo $road->OSM_dest_lat; ?>;
        var dest_lng = <?php echo $road->OSM_dest_lng; ?>;



        function initOSM() {
            // set up the map
            map = new L.Map('OSMdiv');
            // create the tile layer with correct attribution
            var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
            var osm = new L.TileLayer(osmUrl, {minZoom: 10, maxZoom: 19, attribution: osmAttrib});
            // start the map in South-East England
            // map.setView(new L.LatLng(22.4444332,114.0321468),18);
            map.setView(new L.LatLng(origin_lat,origin_lng),18);
            map.addLayer(osm);
        }

        function setOSMMarker(){
            start = [origin_lat,origin_lng ]
            end = [dest_lat,dest_lng ]

            var redIcon = L.icon({
                iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

            L.marker(start).addTo(map);
            L.marker(end, {icon: redIcon}).addTo(map);
        }

        function setPath(){
            var polylinePoints = []
            for (var i = 0; i < paths.length; i++) {
                polylinePoints.push([parseFloat(paths[i]['lat']), parseFloat(paths[i]['lng'])])
            }
            console.log(polylinePoints)

            // var polylinePoints = [
            //     [22.4444332,114.0321468 ],
            //     [22.4444656, 114.0321297],
            //     [22.4445082,114.0321072 ],
            //     [22.4445911, 114.0320684],
            // ];
            var polyline = L.polyline(polylinePoints, {
                color: 'green',
                weight: 5,
                opacity: 0.8,
                smoothFactor: 1
            }).addTo(map);

        }
        function mapInit(){
            initOSM()
            amapInit()
            setOSMMarker()
            setPath()
        }

        function amapInit(){

            var map = new AMap.Map("amapMap", {
                resizeEnable: true,
                center: [amap_origin_lng,amap_origin_lat],//地图中心点
                zoom: 13 //地图显示的缩放级别
            });
            // AMap.event.addListener(map,'click',getLnglat); //点击事件
            var path = [];
            path.push(new AMap.LngLat(amap_origin_lng,amap_origin_lat));
            path.push(new AMap.LngLat(amap_dest_lng,amap_dest_lat));
            map.plugin("AMap.DragRoute",function(){

                route = new AMap.DragRoute(map, path, AMap.DrivingPolicy.LEAST_FEE); //构造拖拽导航类，传入参数分别为：地图对象，初始路径，驾车策略
                console.log("map.plugin")
                route.search(); //查询导航路径并开启拖拽导航

                // console.log();

                AMap.event.addListener(route, "complete", driving_routeCallBack);
                AMap.event.addListener(route, "complete", driving_routeCallBack);

            });
        }




        //构造路线导航类

        function sendNotExist() {
            document.getElementById("exist").value = "0";
            document.getElementById("edit_from").submit();
        }

        function submitAndNext() {
            document.getElementById("next").value = "1";
            document.getElementById("edit_from").submit();
        }

        function sendNotExistAndNext() {
            document.getElementById("next").value = "1";
            document.getElementById("exist").value = "0";
            document.getElementById("edit_from").submit();
        }



        function driving_routeCallBack(route) {
            // body...
			var origin = route['data']['origin']
            var destination = route['data']['destination']
			console.log(route['data'])
			var detail = route['data']['routes'][0]
			var distance = detail['distance']
            var i;
            var steps = detail['steps']
			var roadName = "";
            for (i = 0; i < steps.length; i++) {
                roadName += steps[i]['road'];
                if(i != steps.length -1)
                    roadName+=";"
            }


            var amap_orgin_lat = origin['lat']
            var amap_orgin_lng = origin['lng']
            var amap_dest_lat = destination['lat']
            var amap_dest_lng = destination['lng']
			document.getElementById("amapName").value = roadName;
            document.getElementById("amapLength").value = distance;
            document.getElementById("amapOriginLat").value = amap_orgin_lat;
            document.getElementById("amapOriginLng").value = amap_orgin_lng;
            document.getElementById("amapDestLat").value = amap_dest_lat;
            document.getElementById("amapDestLng").value = amap_dest_lng;

        }

        // console.log(map)

	</script>

</head>

<style type="text/css">
	#amapMap {
		width: 800px;
		height: 300px;
	}

	#OSMdiv {
		width: 800px;
		height: 300px;
	}
</style>

<body onload="mapInit();">

    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="view">Home</a></li>
                                <li><span>View Road Detail</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
										<div class="row">
											<div class="col-8">
                                        		<h4 class="header-title">View Road Detail</h4>
											</div>
											<div class="col-4">
												<a style=" width: inherit; background-color: #269A9A"   href='<?php echo base_url()?>index.php/amap/viewDurationById?roadId=<?php echo $road->id?>' class="btn btn-primary mt-4 pr-4 pl-4">View Records</a>
											</div>
										</div>
                                        <div class="form-group">
                                            <form id='edit_from'method="POST" action="<?php echo base_url()?>index.php/amap/editSubmit">
                                                <?php echo "<input type='hidden' name='roadId' value='{$road->id}'/>"?>
												<?php echo "<input type='hidden' name='exist' id='exist' value='1'/>"?>
												<?php echo "<input type='hidden' name='next' id='next' value='0'/>"?>
                                            	<div class="row">
    	                                        	<div class="col-6">
    	                                        		<label for="OSNName" class="col-form-label">OSM Name</label>
    	                                            	<input class="form-control" type="text" value="<?php echo $road->OSMName;?>" name='OSNName' id="OSNName" disabled>
    	                                        	</div>
													<div class="col-6">
														<label for="OSMLength" class="col-form-label">OSM Length</label>
														<input class="form-control" type="text" value="<?php echo $road->OSMLength;?>" name='OSMLength' id="OSMLength" readonly>
													</div>
                                            	</div>

												<div class="row">
													<div class="col-3">
														<label for="OSMOriginLat" class="col-form-label">OSM Origin Latitude</label>
														<input class="form-control" type="text" value="<?php echo $road->OSM_origin_lat;?>" name='OSMOriginLat' id="OSMOriginLat" readonly>
													</div>
													<div class="col-3">
														<label for="OSMOriginLng" class="col-form-label">OSM Origin longitude</label>
														<input class="form-control" type="text" value="<?php echo $road->OSM_origin_lng;?>" name='OSMOriginLng' id="OSMOriginLng" readonly>
													</div>
													<div class="col-3">
														<label for="OSMDestLat" class="col-form-label">OSM Destination Latitude</label>
														<input class="form-control" type="text" value="<?php echo $road->OSM_dest_lat;?>" name='OSMDestLat' id="OSMDestLat" readonly>
													</div>
													<div class="col-3">
														<label for="OSMDestLng" class="col-form-label">OSM Origin Latitude</label>
														<input class="form-control" type="text" value="<?php echo $road->OSM_dest_lng;?>" name='OSMDestLng' id="OSMDestLng" readonly>
													</div>
                                            	</div>

												<div class="row">
													<div class="col-12">
														<div class="m-2" id="OSMdiv"></div>
													</div>
												</div>

												<div class="row">
													<div class="col-6">
														<label for="amapName" class="col-form-label">AMAP Name</label>
														<input class="form-control" type="text" value="<?php
														if(isset($road->amapName)) echo $road->amapName;?>" name='amapName' id="amapName" >
													</div>
													<div class="col-6">
														<label for="amapLength" class="col-form-label">AMAP Length</label>
														<input class="form-control" type="text" value="<?php
														if(isset($road->amapLength)) echo $road->amapLength;?>" name='amapLength' id="amapLength" readonly>
													</div>



												</div>

												<div class="row">
													<div class="col-3">
														<label for="amapOriginLat" class="col-form-label">AMAP Origin Latitude</label>
														<input class="form-control" type="text" value="<?php echo $road->amap_origin_lat;?>" name='amapOriginLat' id="amapOriginLat" readonly>
													</div>
													<div class="col-3">
														<label for="amapOriginLng" class="col-form-label">AMAP Origin longitude</label>
														<input class="form-control" type="text" value="<?php echo $road->amap_origin_lng;?>" name='amapOriginLng' id="amapOriginLng" readonly>
													</div>
													<div class="col-3">
														<label for="amapDestLat" class="col-form-label">AMAP Destination Latitude</label>
														<input class="form-control" type="text" value="<?php echo $road->amap_dest_lat;?>" name='amapDestLat' id="amapDestLat" readonly>
													</div>
													<div class="col-3">
														<label for="amapDestLng" class="col-form-label">AMAP Origin Latitude</label>
														<input class="form-control" type="text" value="<?php echo $road->amap_dest_lng;?>" name='amapDestLng' id="amapDestLng" readonly>
													</div>
												</div>

												<div class="row">
													<div class="col-12">
														<div class="m-2" id="amapMap"></div>
													</div>
												</div>


                                            	
                                            	<div class="row">
													<div class="col-3">

														<button style=" width: inherit; background-color: #d9534f" type="button" onclick="sendNotExistAndNext()" class="btn btn-primary mt-4 pr-4 pl-4">Not Exist And Next</button>

													</div>
                                            		<div class="col-3">
														<button style=" width: inherit; background-color: #269A9A" type="button" onclick="submitAndNext()" class="btn btn-primary mt-4 pr-4 pl-4">Submit And Next</button>
													</div>
                                            		<div class="col-3">
														<button style=" width: inherit; background-color: #d9534f" type="button" onclick="sendNotExist()" class="btn btn-primary mt-4 pr-4 pl-4">Not Exist</button>
                                            		</div>
                                            		<div class="col-3"><button style=" width: inherit; background-color: #269A9A" type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
                                                    </div>
                                            	</div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
</body>

</html>
