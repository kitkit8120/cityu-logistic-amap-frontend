<!doctype html>
<html class="no-js" lang="en">
<?php
      $this->load->view('alert');
?>
<script>
  $(function() {
    $('.toggle-status').change(function() {
        $btnStatus = 0;
        if($(this).prop('checked')){
            $btnStatus = 1
        }else{
            $btnStatus = 0
        }
        $.ajax({
            url: "<?php echo base_url();?>index.php/users/updateStatus",
            type: 'POST',
            data: {
              status: $btnStatus,
              driverId : $(this).val(),
            },
            error: function(xhr) {
              alert('Cannot send to controller.');
              console.log(xhr)
            },
            success: function(response) {
                console.log(response)
            
            }
          });
        
    })
  })

  function loadPagination() {
      var roadId = <?php echo $records[0]->id;?>;
      var link = "<?php  echo base_url(); ?>index.php/amap/viewDurationById?roadId="+roadId +"&";
      var pages = <?php echo $pages; ?>;
      var page = <?php echo $page; ?>;
      // alert(link)
      document.getElementById('pagination').innerHTML = createPagination(pages, page,link);
  }

</script>


<body onload="loadPagination()">

    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                    </div>

                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="view">Home</a></li>
<!--								--><?php //$editLink = base_url()."index.php/amap/edit?roadId=".$road->id; ?>
								<li><a href="<?php echo base_url()."index.php/amap/edit?roadId=".$records[0]->id; ?>" >Edit Road Link: <?php echo $records[0]->id;?></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- Primary table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <p class="header-title h4 col">AMAP</p>
                                    <div class="header-title icon-container h4 col text-right">

                                    </div>
                                </div>

								<div id="pagination"></div>


								<table class="table text-center">
                                        <thead class="text-capitalize">
                                            <tr>
                                                <th>id</th>
                                                <th>Length</th>
                                                <th>Duration(s)</th>
                                                <th>Create Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($records as $record){
                                                    $s = "<tr>";
                                                    $s .="<td>{$record->id}</td>";
                                                    $s .="<td>{$record->distance}</td>";
													$s .="<td>{$record->duration}</td>";
													$s .="<td>{$record->createDate}</td>";

                                                    echo $s;
                                                }
                                            ?>
										</tbody>
								</table>
							</div>

                        </div>
                    </div>
                    <!-- Primary table end -->

                </div>
            </div>
        </div>
    </div>




</body>

</html>
