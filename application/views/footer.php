<!-- jquery latest version -->
<!-- bootstrap 4 js -->
<script src="<?php echo base_url(); ?>public/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.slicknav.min.js"></script>

<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>

<!-- others plugins -->
<script src="<?php echo base_url(); ?>public/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>public/js/scripts.js"></script>

<script src="<?php echo base_url(); ?>public/js/pagination.js"></script>
<link  rel="stylesheet" href="<?php echo base_url(); ?>public/css/pagination.css">
