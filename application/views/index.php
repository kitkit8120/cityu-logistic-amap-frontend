<!doctype html>
<html class="no-js" lang="en">

<?php 

?>
<body>

    <div id="preloader">
        <div class="loader"></div>
    </div>
    <script src="<?php echo base_url(); ?>public/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>public/js/scripts.js"></script>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                    </div>

                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.php">Home</a></li>
                                <li><span><?php echo $title;?> Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="./api_interface.php?function=logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- Primary table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <p class="header-title h4 col"><?php echo $title?></p>
                                    <div class="header-title icon-container h4 col text-right">
                                        <a href="./create_course.php">
                                            <span class="h5 ti-plus p-2"></span><span class="icon-name m-0 ml-4">Create a <?php echo $title?></span>
                                        </a>

                                    </div>
                                </div>
                                <div class="data-tables datatable-primary">
                                    <table id="dataTable" class="text-center">
                                        <thead class="text-capitalize">
                                            <tr>
                                                <th>Course Code</th>
                                                <th>Animal category</th>
                                                <th>Model Type</th>
                                                <th>Create date</th>
                                                <th>Start date</th>
                                                <th>End Date</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($courses as $course){
                                                    $editLink = "./edit_course.php?course_id=".$course['id'];
                                                    $deleteLink = "./api_interface.php?function=delete_course&course_id=".$course['id'];
                                                    $s = "<tr>";
                                                    $s .="<td>{$course['login_code']}</td>";
                                                    $s .="<td>{$course['eng_name']}</td>";
                                                    $s .="<td>{$course['model_name']}</td>";
                                                    $s .="<td>{$course['create_date']}</td>";
                                                    $s .="<td>{$course['start_date']}</td>";
                                                    $s .="<td>{$course['end_date']}</td>";
                                                    $s .="<td class='fw-icons'><a href='{$editLink}'><span class='fa fa-edit'></span></a></td>";
                                                    $s .="<td class='fw-icons'><a href='{$deleteLink}'><span class='fa fa-remove'></span></a></td>";
                                                    echo $s;
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Primary table end -->

                </div>
            </div>
        </div>
    </div>

</body>

</html>
