

<body>
<?php
$this->load->view('alert');
?>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area login-bg">
        <div class="container">
            <div class="login-box ptb--100">
                <form id='login_form' method="POST" action="<?php echo base_url(); ?>index.php/users/login">
                    <div class="login-form-head">
                        <h4>Sign In</h4>
                        <p>Hello there, Sign in and start managing your Admin</p>
                    </div>
                    <div class="login-form-body">
                            <input type="hidden" name="function" value="login"/>
                            <div class="form-gp">
                                <label for="exampleInputEmail1">User name</label>
                                <input type="text" id="username" name="username">
                                <i class="ti-email"></i>
                            </div>
                            <div class="form-gp">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" id="password" name="password">
                                <i class="ti-lock"></i>
                            </div>

                            <div class="submit-btn-area">
                                <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- others plugins -->
<script src="<?php echo base_url(); ?>public/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>public/js/scripts.js"></script>
</body>



</html>
