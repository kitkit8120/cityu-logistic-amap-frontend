<!doctype html>
<html class="no-js" lang="en">
<?php
      $this->load->view('alert');
?>
<script>
  $(function() {
    // $('.toggle-status').change(function() {

    //     $btnStatus = 0;
    //     if($(this).prop('checked')){
    //         $btnStatus = 1
    //     }else{
    //         $btnStatus = 0
    //     }
    //     $.ajax({
    //         url: "<?php echo base_url();?>index.php/users/updateStatus",
    //         type: 'POST',
    //         data: {
    //           status: $btnStatus,
    //           driverId : $(this).val(),
    //         },
    //         error: function(xhr) {
    //           alert('Cannot send to controller.');
    //           console.log(xhr)
    //         },
    //         success: function(response) {
    //             console.log(response)
            
    //         }
    //       });
        
    // })
  })
</script>


<body>

    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                    </div>

                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="view">Home</a></li>
                                <li><span>Orders Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- Primary table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <p class="header-title h4 col">Orders</p>
                                    <div class="header-title icon-container h4 col text-right">
                                        <a href="<?php echo base_url();?>index.php/orders/create">
                                            <span class="h5 ti-plus p-2"></span><span class="icon-name m-0 ml-4">Create New Order</span>
                                        </a>

                                    </div>
                                </div>
                                <div class="data-tables datatable-primary">
                                    <table id="dataTable" class="text-center">
                                        <thead class="text-capitalize">
                                            <tr>
                                                <th>Invoice No.</th>
                                                
                                                <th>Company</th>
                                                <th>Client Name</th>
                                                <th>Contact</th>
                                                <th>Address</th>
                                                <th>Product Capacity(KG)</th>
                                                <th>Delivery Date</th>
                                                <th>Delivery Time Start</th>
                                                <th>Delivery Time End</th>
                                                <th>Status</th>
                                                <th>Create Date</th>
                                                <th>Update Date</th>
                                                <th>Edit</th>
                                                <th>Delete</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($orders as $order){
                                                    $editLink = base_url()."index.php/orders/edit?orderId=".$order->orderId;

                                                    $deleteLink = base_url()."index.php/orders/deleteSubmit?orderId=".$order->orderId;
                                                    $s = "<tr>";
                                                    $s .="<td>{$order->invoiceNo}</td>";
                                                    
                                                    $s .="<td>{$order->company}</td>";
                                                    $s .="<td>{$order->clientName}</td>";
                                                    $s .="<td>{$order->clientPhone1}</td>";
                                                    $s .="<td>{$order->address}</td>";
                                                    $s .="<td>{$order->capacity}KG</td>";
                                                    $s .="<td>{$order->deliveryDate}</td>";
                                                    $s .="<td>{$order->deliveryTimeStart}</td>";
                                                    $s .="<td>{$order->deliveryTimeEnd}</td>";
                                                    $s .="<td>{$order->status}</td>";
                                                    $s .="<td>{$order->createDate}</td>";
                                                    $s .="<td>{$order->updateDate}</td>";
                                                    $s .="<td class='fw-icons'><a href='{$editLink}'><span class='fa fa-edit'></span></a></td>";
                                                    $s .="<td class='fw-icons'><a href='{$deleteLink}'><span class='fa fa-remove'></span></a></td>";
                                                    echo $s;
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Primary table end -->

                </div>
            </div>
        </div>
    </div>




</body>

</html>
