<!doctype html>
<html class="no-js" lang="en">

<head>
    
<script>
$(function() {
    $('#deliveryDate').datetimepicker({format:'YYYY-MM-DD', defaultDate: new Date(),});
    $('#deliveryTimeStart').datetimepicker({format:'HH:mm:ss' ,defaultDate: '2019-12-30 09:00:00',});
        $('#deliveryTimeEnd').datetimepicker({
            format:'HH:mm:ss',
            useCurrent: false,
            defaultDate: '2019-12-30 18:00:00',

        });
        $("#deliveryTimeStart").on("change.datetimepicker", function (e) {
            $('#deliveryTimeEnd').datetimepicker('minDate', e.date);
        });
        $("#deliveryTimeEnd").on("change.datetimepicker", function (e) {
            $('#deliveryTimeStart').datetimepicker('maxDate', e.date);
        });


})


</script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>

    <div class="modal fade text-center" id="theModal">
     <div class="modal-dialog">
      <div class="modal-content">
      </div>
     </div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="view">Home</a></li>
                                <li><span>Create an Order</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-12 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title">Create an Order</h4>
                                        <div class="form-group">
                                            <form method="POST" action="<?php echo base_url()?>index.php/orders/createSubmit">

                                                <?php echo "<input type='hidden' name='areaId' value='1'/>"?>
                                                <?php echo "<input type='hidden' name='categoryId' value='1'/>"?>

                                            	<div class="row">
    	                                        	<div class="col-4">
    	                                        		<label for="invoiceNo" class="col-form-label">Invoice No.</label>
    	                                            	<input class="form-control" type="text" value="" name='invoiceNo' id="invoiceNo" >
    	                                        	</div>

                                                    <div class="col-2">
                                                        <label for="invoiceCode" class="col-form-label">Invoice Code</label>
                                                        <input class="form-control" type="text" value="" name='invoiceCode' id="invoiceCode" >
                                                    </div>

                                                    <div class="col-6">
                                                        <label for="company" class="col-form-label">Company Name</label>
                                                        <input class="form-control" type="text" value="" name='company' id="company" >
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6">
                                                        <label for="clientName" class="col-form-label">Client Name</label>
                                                        <input class="form-control" type="text" value="" name='clientName' id="clientName" >
                                                    </div>

                                                    <div class="col-3">
                                                        <label for="clientPhone1" class="col-form-label">Client Phone #1</label>
                                                        <input class="form-control" type="number" value="" name='clientPhone1' id="clientPhone1" >
                                                    </div>

                                                    <div class="col-3">
                                                        <label for="clientPhone2" class="col-form-label">Client Phone #2</label>
                                                        <input class="form-control" type="number" value="" name='clientPhone2' id="clientPhone2" >
                                                    </div>

                                                    
                                                </div>

                                                <div class='row'>
                                                    <div class="col-9">
                                                        <label for="address" class="col-form-label">Address(for reference)</label>
                                                        <input class="form-control" type="text" value="" name='address' id="address" >
                                                    </div>
                                                    <div class="col-3">                          
                                                        <button type="button" style=''class="btn btn-success bottom-right-align-text mr-3 ml-3" data-toggle="modal" data-target="#myModal" name="selectBtn">
                                                          Select Latitude and Longitude
                                                        </button>
                                                    </div>

                                                </div>

                                                <div class='row'>
                                                    <div class="col-6">
                                                        <label for="latutide" class="col-form-label">Latitude</label>
                                                        <input class="form-control" type="text" value="" name='latitude' id="latitude" readonly>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="longitude" class="col-form-label">Longitude</label>
                                                        <input class="form-control" type="text" value="" name='longitude' id="longitude" readonly>
                                                        
                                                    </div>

                                                </div>

    	                                        <div class="row">
                                                    <div class="col-4">
                                                        <label for="productName" class="col-form-label">Product Name</label>
                                                        <input class="form-control" type="text" value="" name='productName' id="productName" required="">
                                                    </div>
                                                    <div class="col-4">
                                                        <label for="productNumber" class="col-form-label">Product Code</label>
                                                        <input class="form-control" type="text" value="" name='productNumber' id="productNumber" >
                                                    </div>
                                                    <div class="col-4">
                                                        <label for="charge" class="col-form-label">Any Charge</label>
                                                        <input class="form-control" type="number" value="" name='charge' id="charge" >
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6">
                                                        <label for="capacity" class="col-form-label">Product Capacity (KG)</label>
                                                        <input class="form-control" type="number" value="1" name='capacity' id="capacity">
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="remark" class="col-form-label">Remark</label>
                                                        <input class="form-control" type="text" value="" name='remark' id="remark">
                                                    </div>
                                                </div>
                                            	
                                            	<div class="row">
                                                    <div class="col-4">
                                                        <label class="col-form-label">Delivery Date</label>
                                                        <div class="form-group">
                                                           <div class="input-group date" id="deliveryDate" data-target-input="nearest">
                                                                <input type="text" class="form-control datetimepicker-input" data-target="#deliveryDate" name="deliveryDate" />
                                                                <div class="input-group-append" data-target="#deliveryDate" data-toggle="datetimepicker">
                                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <label class="col-form-label">Start Time</label>
                                                        <div class="form-group">
                                                           <div class="input-group date" id="deliveryTimeStart" data-target-input="nearest">
                                                                <input type="text" class="form-control datetimepicker-input" data-target="#deliveryTimeStart" name="deliveryTimeStart" />
                                                                <div class="input-group-append" data-target="#deliveryTimeStart" data-toggle="datetimepicker">
                                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <label class="col-form-label">End Time</label>
                                                        <div class="form-group">
                                                           <div class="input-group date" id="deliveryTimeEnd" data-target-input="nearest">
                                                                <input type="text" class="form-control datetimepicker-input" data-target="#deliveryTimeEnd" name="deliveryTimeEnd" />
                                                                <div class="input-group-append" data-target="#deliveryTimeEnd" data-toggle="datetimepicker">
                                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            	
                                            	<div class="row">
                                            		<div class="col-5"></div>
                                            		<div class="col-4">
                                            			
                                            		</div>
                                            		<div class="col-3"><button style=" width: inherit; background-color: #269A9A" type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
                                                    </div>
                                            	</div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
</body>

</html>
