<!doctype html>
<html class="no-js" lang="en">
<?php
      $this->load->view('alert');
?>
<script>
  $(function() {
    
  })
</script>


<body>

    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                    </div>

                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.php">Home</a></li>
                                <li><span>Deliveries Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- Primary table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <p class="header-title h4 col">Deliveries</p>
                                    <div class="header-title icon-container h4 col text-right">
                                        <a href="" data-toggle="modal" data-target="#generateModal">
                                            <span class="h5 ti-plus p-2"></span><span class="icon-name m-0 ml-4">Generate Delivery List</span>
                                        </a>

                                    </div>
                                </div>
                                <div class="data-tables datatable-primary">
                                    <table id="dataTable" class="text-center">
                                        <thead class="text-capitalize">
                                            <tr>
                                                <th>Delivery Date</th>
                                                <th>Delivery Time</th>
                                                <th>Order invoice No.</th>
                                                <th>Driver</th>
                                                <th>Vehicle Code</th>
                                                <th>Status</th>
                                                <th>Create Date</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                foreach ($deliveries as $delivery){
                                                    // print_r($delivery);
                                                    $s = "<tr>";

                                                    $s .="<td>{$delivery->deliveryCalDate}</td>";
                                                    $s .="<td>{$delivery->deliveryTime}</td>";
                                                    $s .="<td>{$delivery->invoiceNo}</td>";
                                                    $s .="<td>{$delivery->firstName} {$delivery->lastName}</td>";
                                                    $s .="<td>{$delivery->vehicleCode}</td>";
                                                    $s .="<td>{$delivery->status}</td>";
                                                    $s .="<td>{$delivery->createDate}</td>";

                                                    echo $s;
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Primary table end -->

                </div>
            </div>
        </div>
    </div>




</body>

</html>
