<!doctype html>
<html class="no-js" lang="en">
<?php
      $this->load->view('alert');
?>
<script>
<?php 
    $drivers_array = json_encode($drivers);
    echo "var drivers = ". $drivers_array . ";\n";

    $vehicles_array = json_encode($vehicles);
    echo "var vehicles = ". $vehicles_array . ";\n";
?>

$(document).ready(function () {

    var counter = 1;

    $("#addrow").on("click", function () {
        counter++;
        var newRow = $("<tr>");
        var cols = "";
        var selectDriverName = "slpkDriver"+ counter
        var selectVehicleName = "slpkVehicle"+ counter

        cols += "<td><select id="+selectDriverName+" name="+selectDriverName+" class='selectpicker form-control' style='width:auto !important;' data-live-search='true' ></select></td>"
        cols += "<td><select id="+selectVehicleName+" name="+selectVehicleName+" class='selectpicker form-control' style='width:auto !important;' data-live-search='true' ></select></td>"
        cols += '<td><input type="button" class="ibtnDel btn btn-danger" value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);

        inputDriverSelect(selectDriverName);
        inputVehicleSelect(selectVehicleName);

        $('#'+selectDriverName).selectpicker('refresh');
        $('#'+selectVehicleName).selectpicker('refresh');
    });

    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });

   

    inputDriverSelect("slpkDriver1");
    inputVehicleSelect("slpkVehicle1");


    
});

function inputDriverSelect(name){
    console.log("inputDriverSelect");
    var select = $("#"+name);
    var text ="";
    for (i = 0; i < drivers.length; i++) { 
      text += "<option value='"+drivers[i]['driverId']+"'>"+drivers[i]['firstName'] + " "+ drivers[i]['lastName']+"</option>";
    }
    select.append(text);
}

function inputVehicleSelect(name){
    var select = $("#"+name);
    var text = "";
    for (i = 0; i < vehicles.length; i++) { 
      text += "<option value='"+vehicles[i]['vehicleId']+"'>"+vehicles[i]['vehicleCode']+"</option>";
      
    }
    select.append(text);
}




</script>


<body>

    <div id="preloader">
        <div class="loader"></div>
    </div>

    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">

        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                    </div>

                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.php">Home</a></li>
                                <li><span>Deliveries Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <!-- <img class="avatar user-thumb" src="assets/images/author/avatar.png" alt="avatar"> -->
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Administrator<i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="<?php echo base_url()?>index.php/users/logout">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- Primary table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <form id='edit_from' method="POST" action="<?php echo base_url()?>index.php/deliveries/generateDeliverySubmit">
                                    <div class="row">
                                        <p class="header-title h4 col">Generate <?php echo $targetGenerateDate?> Delivery List</p>
                                        <div class="header-title icon-container h4 col text-right">
                                            Select Drivers and Vehicles for <?php echo $targetGenerateDate?> Order
                                        </div>
                                    </div>
                                    <div class="data-tables datatable-primary">
                                        <table id="myTable" class="text-center table order-list">
                                            <thead class="text-capitalize">
                                                <tr>
                                                    <th>Driver</th>
                                                    <th>Vehicle</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select id="slpkDriver1" name='slpkDriver1' class="selectpicker form-control" style="width:auto !important;" data-live-search="true" ></select>
                                                    </td>
                                                    <td>
                                                        <select id="slpkVehicle1" name='slpkVehicle1' class="selectpicker form-control" style="width:auto !important;"  data-live-search="true" ></select>
                                                    </td>
                                                    <td><a class="deleteRow"></a>

                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: left;">
                                                        <input type="button" class="btn btn-lg btn-block " id="addrow" value="Add Row" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                    <div class="row">
                                		<div class="col-5"></div>
                                		<div class="col-4">
                                			
                                		</div>
                                		<div class="col-3"><button style=" width: inherit; background-color: #269A9A" type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Next</button>
                                        </div>
                                	</div>
                            </div>
                        </div>
                    </div>
                    <!-- Primary table end -->

                </div>
            </div>
        </div>
    </div>




</body>

</html>
