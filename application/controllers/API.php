<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
		$this->load->model('server_model');
		$this->load->model('amap_model');
		$this->load->library('jsonResponse');

    }

	public function getAmapKeyByIp()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$amapKeySet = $this->servers_model->getAmapKeyIdByIp($ip);
		$response = new JsonResponse();
		$response->setValue(true,$amapKeySet);
		$response->printJson();
	}

	public function getRoadSetByIp()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$serverId = $this->servers_model->getServerIdByIp($ip);
		$roadSet = $this->amap_model->getAmapRequestRoadByServerId($serverId);
		$response = new JsonResponse();
		$response->setValue(true,$roadSet);
		$response->printJson();
	}

	public function testIp()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$data['test']= $ip;
		$this->load->view('test',$data);

	}

	public function insertRoadDurationAndDistance(){
		$foo = file_get_contents("php://input");
		$foo = rtrim($foo, "\0");
		$data = json_decode($foo);
		$data_array = $data->data;


	}


}
