<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
		$this->load->library('jsonResponse');
		$this->load->helper('login_helper');
    }

	public function index()
	{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}


	public function login(){
		$form_data = $this->input->post();
		$username = $form_data['username'];
		$password = $form_data['password'];
//		$result = $this->user_model->login($username,$password);
		$result = false;
		if($username =='admin' and $password =='password'){
			$result = true;
		}
		if( $result ==false){
			$this->session->set_flashdata('error', 'The account or password incorrect.');
			$this->index();
		}else{
			$newdata = array(
                   'username'  => $username,
                   'logged_in' => TRUE
               );
			$this->session->set_userdata($newdata);
			redirect('/amap/view', 'refresh');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		$this->index();

	}




	public function editDriver(){
		checkLogin($this->session);
		$form_data = $this->input->get();
		$driverId = $form_data['driverId'];
		$data['driver']= $this->driver_model->getByDriverId($driverId);
		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('driver/edit',$data);
		$this->load->view('footer');
	}

	public function editDriverSubmit(){
		checkLogin($this->session);
		$form_data = $this->input->post();
		$driverId = $form_data['driverId'];
		$this->driver_model->update($driverId,$form_data);
		$this->viewDrivers();
	}

	public function deleteDriverSubmit(){
		checkLogin($this->session);
		$form_data = $this->input->get();
		$driverId = $form_data['driverId'];
		$this->driver_model->delete($driverId);
		$this->viewDrivers();
	}

	public function updateStatus(){
		checkLogin($this->session);
		$form_data = $this->input->post();
		$status = $form_data['status'];
		$driverId = $form_data['driverId'];
		$this->driver_model->updateStatus($driverId,$status);
		
	}

	public function createDriver(){
		checkLogin($this->session);
		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('driver/create');
		$this->load->view('footer');
	}

	public function createDriverSubmit(){
		checkLogin($this->session);
		$form_data = $this->input->post();

		$user_data['username'] = $form_data["username"];
		if($this->user_model->getByUsername($user_data['username'])!=false){
			$this->session->set_flashdata('error', 'The username is exist.');
  
		}else{
			$user_data['password'] = "password";
			$user_data['type'] = 1;
			$userId = $this->user_model->insert($user_data);
			$form_data['status'] = 1;
			$form_data['userId'] = $userId;

			unset($form_data['username']);
			$this->driver_model->insert($form_data);
		}
		$this->viewDrivers();
	}



}
