<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edges extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('edge_model');
//		$this->load->helper('login_helper');
    }

	public function getDistance($lat1, $lon1, $lat2, $lon2 ) {

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		return $miles;
	}


	public function getEdgeByNode(){
		$data = json_decode(file_get_contents('php://input'), true);
		$nodes = $data['nodes'];
		$returnsData = array();
		$edgeResult = $this->edge_model->getAllEdge();
		$tempEdgeSet = [];
		// If use get Edge by Node Id, the latency is more longer. Since the response of database is very slow.
		for($i=0; $i<sizeof($nodes)-1; $i++) {
			$tempEdgeSet[] = array($nodes[$i],$nodes[$i+1]);
		}
		foreach ($edgeResult as $edge){
			for($i=0; $i<sizeof($tempEdgeSet); $i++) {
				if($edge->original_nodeId == intval($tempEdgeSet[$i][0]) and $edge->destination_nodeId == intval($tempEdgeSet[$i][1])){
					$returnsData[$edge->id]= array();
					array_splice($tempEdgeSet,$i,1);
					break;
				}
			}
		}
		$roadLinkPaths= $this->edge_model->getEdgePathByEdgeIdIn(array_keys($returnsData));
		foreach ($roadLinkPaths as $temp){
			$returnsData[$temp->id][]  = array($temp->lat,$temp->lng);
		}

		$array = array_values($returnsData);

		header('Content-Type: application/json');
		echo json_encode($array);

	}

	public function getEdgeByEdgeIdSet(){
		$data = json_decode(file_get_contents('php://input'), true);
		$edgeIds = $data['edges'];
//		$returnsData = array();
//		$edgeResult = $this->edge_model->getAllEdge();
//		$tempEdgeSet = [];
//		// If use get Edge by Node Id, the latency is more longer. Since the response of database is very slow.
//		for($i=0; $i<sizeof($nodes)-1; $i++) {
//			$tempEdgeSet[] = array($nodes[$i],$nodes[$i+1]);
//		}
//		foreach ($edgeResult as $edge){
//			for($i=0; $i<sizeof($tempEdgeSet); $i++) {
//				if($edge->original_nodeId == intval($tempEdgeSet[$i][0]) and $edge->destination_nodeId == intval($tempEdgeSet[$i][1])){
//					$returnsData[$edge->id]= array();
//					array_splice($tempEdgeSet,$i,1);
//					break;
//				}
//			}
//		}
		for($i=0; $i<sizeof($edgeIds); $i++) {
			$returnsData[$edgeIds[$i]]= array();
		}
		$roadLinkPaths= $this->edge_model->getEdgePathByEdgeIdIn(array_keys($returnsData));
		foreach ($roadLinkPaths as $temp){
			$returnsData[$temp->id][]  = array($temp->lat,$temp->lng);
		}

		$array = array_values($returnsData);

		header('Content-Type: application/json');
		echo json_encode($array);

	}

	public function getEdgeByBatchNode(){
		$data = json_decode(file_get_contents('php://input'), true);
		$total = 0;
		$edgeResult = $this->edge_model->getAllEdge();
		for($i = 0; $i<sizeof($data);$i++){
			$nodes = $data[$i];
			$tempEdgeSet = [];
			for($i=0; $i<sizeof($nodes)-1; $i++) {
				$tempEdgeSet[] = array($nodes[$i],$nodes[$i+1]);
				$total++;
			}
		}
		echo 'test'.$total;
//		$nodes = $data['nodes'];
//		$returnsData = array();
//		$edgeResult = $this->edge_model->getAllEdge();
//		$tempEdgeSet = [];
//		for($i=0; $i<sizeof($nodes)-1; $i++) {
//			$tempEdgeSet[] = array($nodes[$i],$nodes[$i+1]);
//		}
//		foreach ($edgeResult as $edge){
//			for($i=0; $i<sizeof($tempEdgeSet); $i++) {
//				if($edge->original_nodeId == intval($tempEdgeSet[$i][0]) and $edge->destination_nodeId == intval($tempEdgeSet[$i][1])){
//					$returnsData[$edge->id]= array();
////					unset($tempEdgeSet[$i]);
//					array_splice($tempEdgeSet,$i,1);
//					break;
//				}
//			}
//		}
//		$roadLinkPaths= $this->edge_model->getEdgePathByEdgeIdIn(array_keys($returnsData));
//		foreach ($roadLinkPaths as $temp){
//			$returnsData[$temp->id][]  = array($temp->lat,$temp->lng);
//		}
//
//		$array = array_values($returnsData);
//
//		header('Content-Type: application/json');
//		echo count($data);
//		echo json_encode($data);

	}




}
