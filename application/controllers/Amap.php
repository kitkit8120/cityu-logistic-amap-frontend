<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amap extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('amap_model');
		$this->load->helper('login_helper');
    }


	public function view(){
		checkLogin($this->session);
		$form_data = $this->input->get();
		$limit = 1000;
		$page = isset($form_data['page']) ? $form_data['page'] :1;

		$start = ($page -1) * $limit;
		$data['roads']= $this->amap_model->getManualAdjustRoadSet($start,$limit);
		$totals = $this->amap_model->getCountOfAmapRoads();
		$pages = ceil($totals/$limit);
		$data['pages'] = $pages;
		$data['page'] = $page;
		$data['title']= "AMAP roads are adjusted by manually";
		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('amap/list',$data);
		$this->load->view('footer');
	}

	public function viewProblemsRoad(){
		checkLogin($this->session);

		$data['roads']= $this->getRequiredManualAdjustRoadSet();
		$data['title']= "Problem AMAP roads";
		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('amap/list',$data);
		$this->load->view('footer');
	}



	public function viewDurationById(){
		$form_data = $this->input->get();
		checkLogin($this->session);
		$limit = 1000;
		$page = isset($form_data['page']) ? $form_data['page'] :1;
		$id = $form_data['roadId'];
		$start = ($page -1) * $limit;
		$data['records']= $this->amap_model->getRecordsByIdWithLimit($id,$limit,$start);
		$totals = $this->amap_model->getCountRecordOfAmapRoadId($id);
		$pages = ceil($totals/$limit);
		$data['pages'] = $pages;
		$data['page'] = $page;

		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('amap/durationList',$data);
		$this->load->view('footer');
	}

	public function doEdit($id){
		$exist = $this->amap_model->checkManualAdjust($id);
		if($exist == true)
			$data['road']= $this->amap_model->getAdjustRoadDetailFromManuallyById($id);
		else{
			$exist = $this->amap_model->checkRoadHasAdjust($id);
			if($exist ==true)
				$data['road']= $this->amap_model->getHaveAdjustRoadDetailById($id);
			else
				$data['road']= $this->amap_model->getNoAdjustRoadDetailById($id);
		}
		$data['paths'] = $this->amap_model->getPathByRoadId($id);
		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('amap/edit',$data);
		$this->load->view('footer');
	}

	public function edit(){
		checkLogin($this->session);
		$form_data = $this->input->get();
		$id = $form_data['roadId'];
		$this->doEdit($id);

	}



	public function manualAdjustSubmit(){
		checkLogin($this->session);
		$form_data = $this->input->get();
		$id = $form_data['roadId'];
		$this->amap_model->manualAdjustUpdate($id,1);
		$this->view();
	}

	public function editSubmit(){
		checkLogin($this->session);
		$form_data = $this->input->post();
//
		$roadId = $form_data['roadId'];
		$adjustedReplaceSet = array("id" =>$roadId, 'name'=> $form_data['amapName'],'length'=>$form_data['amapLength'],'origin_lat'=>$form_data['amapOriginLat'],'origin_lng'=>$form_data['amapOriginLng'],'dest_lat'=>$form_data['amapDestLat'],'dest_lng
'=>$form_data['amapDestLng'],"exist" =>$form_data['exist']);

		$this->amap_model->replaceManualAdjusted($adjustedReplaceSet);

//		$roadLinkReplaceSet = array("id" =>$roadId, 'amapLength'=>$form_data['amapLength'],'amapDuration'=>$form_data['amapDuration'],'amapName'=>$form_data['amapName']);
//		$this->amap_model->replaceRoadLink($roadLinkReplaceSet);
//		$this->amap_model->manualAdjustUpdate($roadId,1);

		if($form_data['next'] == '1'){
			$roads = $this->getRequiredManualAdjustRoadSet();
			$rand_roads = array_rand($roads, 2);
//			$data['test']= $roads[$rand_roads[0]]->id;
//			$this->load->view('test',$data);

			$this->doEdit($roads[$rand_roads[0]]->id);
		}else{
			$this->view();
		}
	}

	public function getRequiredManualAdjustRoadSet(){

		$needAdjustRoadArray = array();
		$totalManualAdjustSet = array();
		$latestDate = $this->amap_model->getLatestComparedUpdateTime()->updateDate;
		$needAdjustRoadSet =$this->amap_model->getProblemRoadSet($latestDate);
		foreach ($needAdjustRoadSet as $row){
			array_push($needAdjustRoadArray, $row->id);
		}
		$haveManualRoadSet= $this->amap_model->getProblemManualRoadSetById($needAdjustRoadArray);
		$haveManualRoadIdArray = array();
		foreach ($haveManualRoadSet as $row){
			if($row->updateDate >$latestDate){
				array_push($haveManualRoadIdArray, $row->id);
			}else{
				array_push($totalManualAdjustSet, $row);
			}
		}
		$APIProblemAdjustedRoadSet = $this->amap_model->getProblemAPIAdjustRoadSetByIdLimit50($needAdjustRoadArray,$haveManualRoadIdArray);
		foreach ($APIProblemAdjustedRoadSet as $row){
			array_push($totalManualAdjustSet, $row);
		}

		return $totalManualAdjustSet;


	}

	public function deleteSubmit(){
		checkLogin($this->session);
		$form_data = $this->input->get();
		$vehicleId = $form_data['vehicleId'];
		$this->vehicle_model->delete($vehicleId);
		$this->view();
	}

	public function updateStatus(){
		$form_data = $this->input->post();
		
		$status = $form_data['status'];
		$vehicleId = $form_data['vehicleId'];
		$this->vehicle_model->updateStatus($vehicleId,$status);
		
	}

	public function create(){
		checkLogin($this->session);
		$this->load->view('header');
		$this->load->view('side_menu');
		$this->load->view('vehicle/create');
		$this->load->view('footer');
	}

	public function createSubmit(){
		$form_data = $this->input->post();

		if($this->vehicle_model->getByVehicleCode($form_data['vehicleCode'])!=false){
			$this->session->set_flashdata('error', 'The vehicle code is exist.');
  
		}else{
			
			$this->vehicle_model->insert($form_data);
		}
		$this->view();
	}



}
