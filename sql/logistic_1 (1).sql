-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2019 at 05:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logistic_1`
--
CREATE DATABASE IF NOT EXISTS `logistic_1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `logistic_1`;

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `areasId` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`areasId`, `name`) VALUES
(1, '九龍西'),
(2, '港島東'),
(3, '港島西'),
(4, '九龍東'),
(5, '新界西'),
(6, '東涌'),
(7, '新界東');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL,
  `name` text NOT NULL,
  `height` double NOT NULL,
  `width` double NOT NULL,
  `depth` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `name`, `height`, `width`, `depth`) VALUES
(1, '最小型家品', 40, 40, 40),
(2, '被', 60, 60, 30),
(3, '食品箱', 50, 35, 40),
(4, '大型家品', 80, 60, 60),
(5, '璧畫', 120, 10, 80),
(6, '中型家品', 60, 30, 40),
(7, '中型傢私', 80, 80, 160),
(8, '小傢俱', 70, 60, 60);

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

DROP TABLE IF EXISTS `deliveries`;
CREATE TABLE `deliveries` (
  `deliveryId` int(11) NOT NULL,
  `driverId` int(11) NOT NULL,
  `vehicleId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `deliveryDate` date NOT NULL,
  `deliveryTime` time NOT NULL,
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

DROP TABLE IF EXISTS `drivers`;
CREATE TABLE `drivers` (
  `driverId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `phoneNumber` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`driverId`, `userId`, `firstName`, `lastName`, `createDate`, `updateDate`, `phoneNumber`, `status`) VALUES
(2, 3, '4', '5', '2019-08-23 17:17:29', '2019-08-26 16:41:51', 6, 1),
(3, 4, '45123', '5213', '2019-08-23 17:17:44', '2019-08-26 16:41:52', 5213, 1),
(4, 6, '45123', '5213', '2019-08-23 17:18:38', '2019-08-26 16:41:53', 5213, 1),
(5, 7, '56123', '6', '2019-08-23 17:33:14', '2019-08-26 16:41:51', 6, 1),
(6, 8, '6', '7', '2019-08-23 17:37:50', '2019-08-26 16:37:38', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `orderId` int(11) NOT NULL,
  `areaId` int(11) NOT NULL,
  `invoiceNo` text NOT NULL,
  `invoiceCode` int(11) NOT NULL,
  `company` text NOT NULL,
  `deliveryDate` date NOT NULL,
  `clientName` text NOT NULL,
  `clientPhone1` text NOT NULL,
  `clientPhone2` text NOT NULL,
  `address` text NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `productName` text NOT NULL,
  `productNumber` int(11) NOT NULL,
  `capacity` double NOT NULL,
  `charge` double NOT NULL,
  `remark` text NOT NULL,
  `categoryId` int(11) NOT NULL,
  `status` text NOT NULL,
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deliveryTimeStart` time NOT NULL,
  `deliveryTimeEnd` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderId`, `areaId`, `invoiceNo`, `invoiceCode`, `company`, `deliveryDate`, `clientName`, `clientPhone1`, `clientPhone2`, `address`, `latitude`, `longitude`, `productName`, `productNumber`, `capacity`, `charge`, `remark`, `categoryId`, `status`, `createDate`, `updateDate`, `deliveryTimeStart`, `deliveryTimeEnd`) VALUES
(4, 1, 'HODO-2018003143', 65646, '裕華', '2019-09-03', '', '', '', '何文田山道7號俊慧園C座9樓C2', 0, 0, '工藝', 0, 10, 0, 'None', 1, '', '2019-09-03 17:26:55', '2019-09-03 17:28:36', '09:00:00', '18:00:00'),
(5, 1, 'A00496', 0, '裕華', '2019-09-03', '', '', '', '何文田石鼓街2號毅盧13樓D座', 0, 0, '針織', 0, 1, 0, 'None', 1, '', '2019-09-03 17:30:54', '2019-09-03 17:34:18', '09:00:00', '18:00:00'),
(7, 1, 'HODO-2018003094', 54160, '裕華', '2019-09-03', '', '', '', '柴灣利眾街22號光榮工業大樓2樓', 0, 0, '檯布', 1, 1, 0, 'None', 1, '', '2019-09-03 17:34:50', '2019-09-03 17:34:50', '09:00:00', '18:00:00'),
(8, 1, 'A08538', 0, '裕華', '2019-09-03', '', '', '', '柴灣杏花邨43座', 0, 0, '針織', 0, 1, 0, '', 1, '', '2019-09-03 17:36:28', '2019-09-03 17:36:40', '09:00:00', '18:00:00'),
(10, 1, '2613', 0, '裕華', '2019-09-03', '', '', '', ' 黃竹坑業興街11號南匯廣場B座12/F 2309室', 0, 0, '電毯', 0, 1, 0, '', 1, '', '2019-09-03 17:37:25', '2019-09-03 17:37:25', '09:00:00', '18:00:00'),
(11, 1, '2610', 0, '裕華', '2019-09-03', '', '', '', '中環美臣里1號安慶大廈17/F D', 0, 0, '電毯', 0, 1, 0, '', 1, '', '2019-09-03 17:37:51', '2019-09-03 17:37:51', '09:00:00', '18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `username`, `password`, `type`) VALUES
(1, '1', '1', 0),
(2, 'driver01', 'driver01', 1),
(3, '2', 'password', 0),
(4, '51', 'password', 0),
(5, '51', 'password', 0),
(6, '51', 'password', 1),
(7, '', 'password', 1),
(8, '15', 'password', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `vehicleId` int(11) NOT NULL,
  `vehicleCode` text NOT NULL,
  `capacity` double NOT NULL,
  `createDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vehicleId`, `vehicleCode`, `capacity`, `createDate`, `updateDate`, `status`) VALUES
(2, 'CS5123', 0, '2019-08-23 18:14:00', '2019-08-26 16:41:40', 1),
(3, 'GSC21235', 0, '2019-08-23 18:14:28', '2019-08-26 16:41:47', 1),
(4, '5', 5, '2019-08-26 16:44:05', '2019-08-26 16:45:54', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`areasId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`deliveryId`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`driverId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`),
  ADD KEY `areaId` (`areaId`),
  ADD KEY `categoryId` (`categoryId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`vehicleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `areasId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `deliveryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `driverId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `vehicleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `drivers`
--
ALTER TABLE `drivers`
  ADD CONSTRAINT `drivers_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`areaId`) REFERENCES `areas` (`areasId`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
